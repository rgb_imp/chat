const chat_text=document.getElementsByClassName('chat_messages')[0];

setTimeout( function reset() { 
    getRenderMessages();
setTimeout(reset, 1000)}, 1000);

function getRenderMessages() {
    axios.get('/api/chat/message') 
    .then(function(response) {
        const messages=JSON.parse(response.request.response).reverse();
        renderChatMessage(messages);
    })
    .catch(function(error) {
        console.log(error);
    })
}

function renderChatMessage(messages) {
    if (!messages.length) return;

    messages.forEach (function(item) {
        const itemLi = document.getElementById(item.id);
        if (!itemLi){
            addNewMessageToDOM(item);
        } else {
            const divText = itemLi.querySelector('.text')
            if (divText && divText.textContent!==item.text) {
                divText.textContent=item.text;
            }
        }

        
    });
    const array_id=messages.map(function(message){
        return message.id
    })
    Array.from(chat_text.querySelectorAll('.message')).forEach(function(itemLi) {
        if (!array_id.includes(Number(itemLi.id))) {
            chat_text.removeChild(itemLi);
        }          
    });  
}

function addNewMessageToDOM(item) {
    const newMessage = document.createElement('li');
    newMessage.classList = 'message';
    newMessage.setAttribute('id', item.id);
    
    newMessage.innerHTML = createMessageView(item.username, item.text);
    deleteMessage(newMessage, item);
    editMessage(newMessage, item);

    chat_text.appendChild(newMessage);


}

function createMessageView(username, text) {
    return `
    <div class='message_div'>
        <div class='username'>${username}</div>: <div class='text'>${text}</div>
        <div class='edit_button'></div>
        <div class='delete_button'></div>
        </div>
`;
}

const createEditMessageView = (username, text)=>`
<div class='message_div'>
        <div class='username'>${username}</div>:<input type='text' name='edit_message' placeholder="${text}" id='edit_message'>
            <div class='enter_button'></div>
            <div class='cancel_button'></div>
        </div>
`;

//получение имени и сoобщения и вывод в чат
document.getElementById('button_send_message').addEventListener('click',
    function () {
            axios.post('/api/chat/message', {
                username: document.getElementById('user_name').value,
                text: document.getElementById('user_message').value
            })
                .then(function() {
                   document.getElementById('user_name').value = '';     
                   document.getElementById('user_message').value = '';    
                           
                })
                .catch(function(error) {
                    console.log(error);
                });
        }
)    

    //удаление сообщения
function deleteMessage(newMessage, item) {
    newMessage.querySelector('.delete_button').addEventListener('click', function() {
    //
    axios.delete(`api/chat/message/${item.id}`);
    getRenderMessages();
});
}

    //измененеие сообщения
function editMessage(newMessage, item) {
    newMessage.querySelector('.edit_button').addEventListener('click', function() {
        newMessage.innerHTML = createEditMessageView(item.username, item.text);
        newMessage.querySelector('.enter_button').addEventListener('click',
            function() {
                item.text = newMessage.querySelector('#edit_message').value;
                
                axios.put(`api/chat/message/${item.id}`, {
                    username: item.username,
                    text: item.text
                })
                    .then(function(response) {
                        let newItem = response.data;
                        
                        newMessage.innerHTML = createMessageView(newItem.username, newItem.text);
                        deleteMessage(newMessage,newItem);
                        editMessage(newMessage,newItem);
                    })
                    .catch(function(error){
                        console.log(error);
                    })
                ;
                
            }
        );
        newMessage.querySelector('.cancel_button').addEventListener('click', 
            function() {
                axios.get(`/api/chat/message/${item.id}`) 
                .then(function(response) {
                    console.log(response.request.response);
                        newMessage.innerHTML = createMessageView(item.username, item.text);
                        deleteMessage(newMessage,item);
                        editMessage(newMessage,item);
                    })
                .catch(function(error) {
                console.log(error);
                })        
            }
        );
    });
}

